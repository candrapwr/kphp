<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
use DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ServicesController extends Controller
{
    public function add_pohon(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'pohon_nama' => 'required|max:100',
            'pohon_jenis' => 'required|max:50',
            'pohon_diameter' => 'required|numeric',
            'pohon_tinggi' => 'required|numeric',
            'pohon_status' => 'required|max:20',
            'pohon_tahun_tanam' => 'required|numeric|digits:4',
            'pohon_rentang_tumbuh' => 'required|max:10',
            'pohon_lat' => 'required|numeric|between:-90,90',
            'pohon_long' => 'required|numeric|between:-180,180'
        ]);

        if($validator->fails()){
            $errorMsg['success'] = false;
            $errorMsg['messages'] = 'Validator error !';
            $errorMsg['response'] = $validator->errors();
            return response()->json($errorMsg);       
        }

        try
        {
            $dataField = [
                'pohon_nama'=> $request->input('pohon_nama'),
                'pohon_jenis'=> $request->input('pohon_jenis'),
                'pohon_diameter'=> $request->input('pohon_diameter'),
                'pohon_tinggi'=> $request->input('pohon_tinggi'),
                'pohon_status'=> $request->input('pohon_status'),
                'pohon_tahun_tanam'=> $request->input('pohon_tahun_tanam'),
                'pohon_rentang_tumbuh'=> $request->input('pohon_rentang_tumbuh'),
                'pohon_lat'=> $request->input('pohon_lat'),
                'pohon_long'=> $request->input('pohon_long'),
                'created_user'=> auth()->user()->email,
                'created_device'=> 'mobile'
            ];

            if($request->hasFile('pohon_img')){
                $file = $request->file('pohon_img');
                $destinationPath = 'uploads/pohon';
                $fileName = time().'.jpg';
                $file->move($destinationPath,$fileName);
                $dataField['pohon_img'] = $fileName;

                $data = DB::table('pohon')
                ->insert($dataField);
    
                $dataJson['success'] = true;
                $dataJson['messages'] = 'Data added successfully';
                $dataJson['response'] = '';
            }else{
                $dataJson['success'] = false;
                $dataJson['messages'] = 'Data images pohon_img not valid !';
                $dataJson['response'] = '';
            }

        }
        catch(\Illuminate\Database\QueryException $e)
        {
            //dd($e->getMessage());
            $code_er = '#SVC-'.time();
            DB::table('log_error')
            ->insert([
                'error_source'=> 'service',
                'error_msg'=> $e->getMessage(),
                'error_code'=> $code_er
            ]);
            $dataJson['success'] = false;
            $dataJson['messages'] = 'Failed to add data, error '.$code_er;
            $dataJson['response'] = '';
        }

        return response()
            ->json($dataJson);
    }

    public function get_pohon(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id' => 'required',
        ]);

        if($validator->fails()){
            $errorMsg['success'] = false;
            $errorMsg['messages'] = 'Validator error !';
            $errorMsg['response'] = $validator->errors();
            return response()->json($errorMsg);       
        }

        try
        {
            $data = DB::table('pohon')
            ->where('pohon_id', $request->input('id'))
            ->first();
            
            if($data != null){
                $qrcode = QrCode::format('svg')->generate($request->input('id'));
                $data->qr = (String) $qrcode;
                $dataJson['success'] = true;
                $dataJson['messages'] = 'Data found';
                $dataJson['response'] = $data;
            }else{
                $dataJson['success'] = false;
                $dataJson['messages'] = 'Data not found !';
                $dataJson['response'] = (object) [];
            }

        }
        catch(\Illuminate\Database\QueryException $e)
        {
            //dd($e->getMessage());
            $code_er = '#SVC-'.time();
            DB::table('log_error')
            ->insert([
                'error_source'=> 'service',
                'error_msg'=> $e->getMessage(),
                'error_code'=> $code_er
            ]);
            $dataJson['success'] = false;
            $dataJson['messages'] = 'Failed to get data, error '.$code_er;
            $dataJson['response'] = '';
        }

        return response()
            ->json($dataJson);
    }
}
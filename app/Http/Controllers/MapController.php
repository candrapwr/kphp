<?php
 
namespace App\Http\Controllers;
 
use DB;
use Response;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
 
class MapController extends Controller
{
    public function index(){
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Pohon"], ['name' => "BDH"]];
        return view('/content/map/front', ['breadcrumbs' => $breadcrumbs]);
    }

    public function get_poi_geojson(Request $request)
    {
        $sql = "
        SELECT json_build_object(
            'type', 'FeatureCollection',
            'features', json_agg(ST_AsGeoJSON(p.*)::json)
            ) as json
        FROM pohon p 
        ";
        $data = DB::select($sql)[0];
        $response = Response::make($data->json, 200);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function get_poly_geojson(Request $request)
    {
        $sql = "
        SELECT json_build_object(
            'type', 'FeatureCollection',
            'features', json_agg(ST_AsGeoJSON(vdr.*)::json)
            ) as json
        FROM v_detail_rph vdr
        where vdr.rph_geom is not null;   
        ";
        $data = DB::select($sql)[0];
        $response = Response::make($data->json, 200);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function html_pohon(Request $request){
        $data = DB::table('pohon')
        ->where('pohon_id', $request->input('dataId'))
        ->first();
        $qrcode = QrCode::format('svg')->generate($request->input('dataId'));
        $data->qr = (String) $qrcode;
        return view('/content/map/pohon',['data' => $data]);
    }

    public function html_poly(Request $request){
        $data = DB::table('rph')
        ->selectRaw('rph.*,bdh.bdh_name,bdh.bdh_id,kph.kph_name,kph.kph_id,v_jumlah_pohon.total_pohon')
        ->leftjoin('bdh','bdh.bdh_id','=','rph.bdh_id')
        ->leftjoin('kph','kph.kph_id','=','bdh.kph_id')
        ->leftjoin('v_jumlah_pohon','v_jumlah_pohon.rph_id','=','rph.rph_id')
        ->where('rph.rph_id', $request->input('dataId'))
        ->first();
        return view('/content/map/poly',['data' => $data]);
    }
}
var map = L.map('map', {
    zoomControl: false
}).setView([-7.79898677945525, 110.37426800776367], 10);

var tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">Map</a>'
}).addTo(map);

var dasarStyle = {
    "color": "blue",
    "fillColor": "white",
    "weight": 2,
    "opacity": 1,
    "fillOpacity": 0.3,
    "dashArray": '1, 5',
    "dashOffset": '0'
};

var kphStyle = {
    "color": "black",
    "fillColor": "green",
    "weight": 1,
    "opacity": 1,
    "fillOpacity": 0.3,
    "dashArray": '0',
    "dashOffset": '0'
};

// Info Konten ===
var infoKonten = L.control({
    position: 'topleft'
});
infoKonten.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'infoKonten');
    L.DomEvent.disableClickPropagation(this._div);
    L.DomEvent.disableScrollPropagation(this._div);
    return this._div;
};
infoKonten.update = function (str = '') {
    this._div.innerHTML = str;
};
infoKonten.addTo(map);
//===================


function getPoint(url) {
    $.ajax({
        'type': "GET",
        'url': url,
        'data': null,
        'success': function (data) {
            //console.log(data);
            loadGeojsonCluster(data);
        }
    });
}

function getPolyDasar(url, styleData) {
    $.ajax({
        'type': "GET",
        'url': url,
        'data': null,
        'success': function (data) {
            //console.log(data);
            loadGeojson(data, styleData);
            getPoly($('body').attr('data-asset-path') + 'map_poly', kphStyle);
        }
    });
}

function getPoly(url, styleData) {
    $.ajax({
        'type': "GET",
        'url': url,
        'data': null,
        'success': function (data) {
            //console.log(data);
            loadGeojson(data, styleData);
        }
    });
}

function loadGeojson(str, styleData) {
    geojson = new L.geoJson(str, {
        style: styleData,
        onEachFeature: onEachFeature
    }).addTo(map);
    //map.fitBounds(geojson.getBounds());
    props = null;
    //map.setView([-2.7235830833483856, 112.50000000000001], 5.0);
    $(".info").text('Informasi');
}

function loadGeojsonCluster(str) {
    geojson = new L.geoJson(str, {
        style: style,
        onEachFeature: onEachFeature
    });
    var markers = L.markerClusterGroup();
    markers.addLayer(geojson);
    map.addLayer(markers);
    props = null;
    //map.setView([-2.7235830833483856, 112.50000000000001], 5.0);
    $(".info").text('Informasi');
}

function onEachFeature(feature, layer) {
    layer.on('click', function (e) {
        infoKonten.update('');
        //infoKonten.update('<img src="http://localhost:8080/img/loading.gif">');
        if (feature.geometry['type'] != 'Point') {
            map.fitBounds(layer.getBounds());
            if (feature.properties.Provinsi == null) {
                getPolyDetail(feature.properties.rph_id);
            } else {
                //infoKonten.update('xx');
            }
        } else {
            getPohonDetail(feature.properties.pohon_id);
        }
    });
}

function style(feature) {
    return {
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.6,
        fillColor: feature.properties.fillcolor
    };
}

function getPohonDetail(dataId) {
    $.ajax({
        type: "POST",
        url: $('body').attr('data-asset-path') + 'map_detail_pohon',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: 'dataId=' + dataId,
        'success': function (data) {
            //console.log(data);
            infoKonten.update(data);
        }
    });
}

function getPolyDetail(dataId) {
    $.ajax({
        type: "POST",
        url: $('body').attr('data-asset-path') + 'map_detail_poly',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: 'dataId=' + dataId,
        'success': function (data) {
            //console.log(data);
            infoKonten.update(data);
        }
    });
}

getPolyDasar($('body').attr('data-asset-path') + 'js/leaflet/diy.json', dasarStyle);
getPoint($('body').attr('data-asset-path') + 'map_point');
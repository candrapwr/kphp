$(function () {
    'use strict';
    $('.datatables-basic').DataTable({
        ajax: $('body').attr('data-asset-path') + 'pohon/all',
        columns: [{
                data: 'pohon_id'
            },
            {
                data: 'pohon_nama'
            },
            {
                data: 'pohon_jenis'
            },
            {
                data: 'pohon_diameter'
            },
            {
                data: 'pohon_tinggi'
            },
            {
                data: 'pohon_tahun_tanam'
            },
            {
                data: ''
            }
        ],
        columnDefs: [{
                targets: 0,
                responsivePriority: 4,
                visible: false
            },
            {
                targets: 1,
                visible: true
            },
            {
                targets: 2,
                visible: true
            },
            {
                targets: -1,
                title: 'Actions',
                orderable: false,
                render: function (data, type, full, meta) {
                    var idData = full['pohon_id'];
                    return (
                        '<div class="d-inline-flex">' +
                        '<a class="pe-1 dropdown-toggle hide-arrow text-primary" data-bs-toggle="dropdown">' +
                        feather.icons['menu'].toSvg({
                            class: 'font-small-4'
                        }) +
                        '</a>' +
                        '<div class="dropdown-menu dropdown-menu-end">' +
                        '<a href="javascript:void(0);" class="dropdown-item" onClick="detail(\'' + idData + '\')">' +
                        feather.icons['eye'].toSvg({
                            class: 'font-small-4 me-50'
                        }) +
                        'Detail</a>' +
                        '<a href="javascript:void(0);" class="dropdown-item" onClick="hapus(\'' + idData + '\')">' +
                        feather.icons['archive'].toSvg({
                            class: 'font-small-4 me-50'
                        }) +
                        'Delete</a>' +
                        '<a href="javascript:void(0);" class="dropdown-item" onClick="edit(\'' + idData + '\')">' +
                        feather.icons['edit'].toSvg({
                            class: 'font-small-4 me-50'
                        }) +
                        'Edit</a>' +
                        '</div>' +
                        '</div>'
                    );
                }
            }
        ],
        order: [
            //[0, 'desc']
        ],
        dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        displayLength: 10,
        buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({
                    class: 'font-small-4 me-50'
                }) + 'Export',
                buttons: [{
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5]
                        }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5]
                        }
                    }
                ],
                init: function (api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function () {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            },
            {
                text: feather.icons['plus'].toSvg({
                    class: 'me-50 font-small-4'
                }) + 'Tambah Data',
                className: 'create-new btn btn-primary',
                attr: {
                    'data-bs-toggle': 'modal',
                    'data-bs-target': '#modals-insert'
                },
                init: function (api, node, config) {
                    $(node).removeClass('btn-secondary');
                }
            }
        ],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal({
                    header: function (row) {
                        var data = row.data();
                        return 'Details of ' + data['kph_name'];
                    }
                }),
                type: 'column',
                renderer: function (api, rowIdx, columns) {
                    var data = $.map(columns, function (col, i) {
                        return col.title !== '' ?
                            '<tr data-dt-row="' +
                            col.rowIdx +
                            '" data-dt-column="' +
                            col.columnIndex +
                            '">' +
                            '<td>' +
                            col.title +
                            ':' +
                            '</td> ' +
                            '<td>' +
                            col.data +
                            '</td>' +
                            '</tr>' :
                            '';
                    }).join('');

                    return data ? $('<table class="table"/>').append('<tbody>' + data + '</tbody>') : false;
                }
            }
        },
        language: {
            paginate: {
                previous: '&nbsp;',
                next: '&nbsp;'
            }
        }
    });
    $('div.head-label').html('<h6 class="mb-0">List Data</h6>');
    $('#loadingSimpan').hide();
});

function simpan(mode) {
    var data;
    if (mode == 'update') {
        data = new FormData($("#formSimpanE")[0]);
    } else {
        data = new FormData($("#formSimpan")[0]);
    }

    $.ajax({
        url: $('body').attr('data-asset-path') + 'pohon/add',
        type: 'post',
        //dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: data,
        processData: false,
        contentType: false,
        beforeSend: function () {
            $('#loadingSimpan').show();
            $('#btnSimpan').hide();
        },
        complete: function () {
            $('#loadingSimpan').hide();
            $('#btnSimpan').show();
        },
        success: function (responseStr, textStatus, xhr) {
            var response = JSON.parse(responseStr);
            if (response.sukses == true) {
                $('#modals-insert').modal('hide');
                $('#modals-edit').modal('hide');
                Swal.fire(
                    'SUKSES',
                    response.pesan,
                    'success'
                );
                $('.datatables-basic').DataTable().ajax.reload();
            } else {
                Swal.fire(
                    'GAGAL',
                    response.pesan,
                    'error'
                )
            }
        },
        error: function (request, status, error) {
            Swal.fire(
                'GAGAL',
                error,
                'error'
            )
        }
    })
}

function hapus(dataId) {
    Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Data ini akan di hapus permanen!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Hapus!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: $('body').attr('data-asset-path') + 'pohon/del',
                type: 'post',
                dataType: 'json',
                data: {
                    dataId: dataId,
                    _token: $('meta[name="csrf-token"]').attr('content'),
                },
                beforeSend: function () {},
                complete: function () {},
                success: function (response, textStatus, xhr) {
                    console.log(response)
                    if (response.sukses == true) {
                        Swal.fire(
                            'SUKSES',
                            response.pesan,
                            'success'
                        );
                        $('.datatables-basic').DataTable().ajax.reload();
                    } else {
                        Swal.fire(
                            'GAGAL',
                            response.pesan,
                            'error'
                        )
                    }
                },
                error: function (request, status, error) {
                    Swal.fire(
                        'GAGAL',
                        error,
                        'error'
                    )
                }
            })
        }
    })
}

function edit(dataId) {
    var latLong = [];
    $.ajax({
        url: $('body').attr('data-asset-path') + 'pohon/edit',
        type: 'post',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            dataId: dataId,
            _token: $('meta[name="csrf-token"]').attr('content'),
        },
        beforeSend: function () {
            $('#loadingSimpanE').show();
            $('#btnSimpanE').hide();
        },
        complete: function () {
            $('#loadingSimpanE').hide();
            $('#btnSimpanE').show();
        },
        success: function (response, textStatus, xhr) {
            //console.log(response.data);
            $('input[name="pohon_nama_e"]').val(response.data.pohon_nama);
            $('input[name="pohon_jenis_e"]').val(response.data.pohon_jenis);
            $('input[name="pohon_diameter_e"]').val(response.data.pohon_diameter);
            $('input[name="pohon_tinggi_e"]').val(response.data.pohon_tinggi);
            $('select[name="pohon_status_e"]').val(response.data.pohon_status);
            $('input[name="pohon_tahun_tanam_e"]').val(response.data.pohon_tahun_tanam);
            $('select[name="pohon_rentang_tumbuh_e"]').val(response.data.pohon_rentang_tumbuh);
            $('input[name="pohon_lat_e"]').val(response.data.pohon_lat);
            $('input[name="pohon_long_e"]').val(response.data.pohon_long);
            $('input[name="dataId"]').val(response.data.pohon_id);
            $('#modals-edit').modal('show');
            latLong.push(response.data.pohon_lat);
            latLong.push(response.data.pohon_long);
            if (theMarkerE != undefined) {
                mapE.removeLayer(theMarkerE);
            };
            theMarkerE = L.marker(latLong,{
                draggable: true
              }).addTo(mapE);
        },
        error: function (request, status, error) {
            Swal.fire(
                'GAGAL',
                error,
                'error'
            )
        }
    })
}

function detail(dataId) {
    //console.log(APP_URL+'/aa');
    $.ajax({
        url: $('body').attr('data-asset-path') + 'pohon/detail',
        type: 'post',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            dataId: dataId,
            _token: $('meta[name="csrf-token"]').attr('content'),
        },
        beforeSend: function () {
            $('#_foto').html('');
            $('#_qrcode').html('');
        },
        complete: function () {

        },
        success: function (response, textStatus, xhr) {
            //console.log(response.data);
            $('#_pohon_nama').val(response.data.pohon_nama);
            $('#_pohon_jenis').val(response.data.pohon_jenis);
            $('#_pohon_diameter').val(response.data.pohon_diameter);
            $('#_pohon_tinggi').val(response.data.pohon_tinggi);
            $('#_pohon_status').val(response.data.pohon_status);
            $('#_pohon_tahun_tanam').val(response.data.pohon_tahun_tanam);
            $('#_pohon_rentang_tumbuh').val(response.data.pohon_rentang_tumbuh);
            $('#_pohon_long').val(response.data.pohon_long);
            $('#_pohon_lat').val(response.data.pohon_lat);
            if (response.data.pohon_img != null) {
                $('#_foto').html('<img width="100%" src="' + APP_URL + '/uploads/pohon/' + response.data.pohon_img + '">');
            }
            $('#_qrcode').html(response.data.qr);
            $('#modals-detail').modal('show');
        },
        error: function (request, status, error) {
            Swal.fire(
                'GAGAL',
                error,
                'error'
            )
        }
    })
}

$('#modals-insert').on('hidden.bs.modal', function (e) {
    $(this)
        .find("input,textarea,select")
        .val('')
        .end()
        .find("input[type=checkbox], input[type=radio]")
        .prop("checked", "")
        .end();
})

$('#modals-insert').on('shown.bs.modal', function () {
    setTimeout(function () {
        map.invalidateSize();
    }, 10);
});

$('#modals-edit').on('shown.bs.modal', function () {
    setTimeout(function () {
        mapE.invalidateSize();
        //getPolyDasar($('body').attr('data-asset-path') + 'js/leaflet/diy.json',dasarStyle);
    }, 10);
});



var map = L.map('map').setView([-7.79898677945525, 110.37426800776367], 10);
var mapE = L.map('mapE').setView([-7.79898677945525, 110.37426800776367], 10);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: ''
}).addTo(map);
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: ''
}).addTo(mapE);

var theMarker = {};
var theMarkerE = {};

function onMapClick(e) {
    $('#pohon_lat').val(e.latlng.lat);
    $('#pohon_long').val(e.latlng.lng);


    if (theMarker != undefined) {
        map.removeLayer(theMarker);
    };
    theMarker = L.marker([e.latlng.lat, e.latlng.lng],{
        draggable: true
      }).addTo(map);
}

function onMapClickE(e) {
    $('#pohon_lat_e').val(e.latlng.lat);
    $('#pohon_long_e').val(e.latlng.lng);


    if (theMarkerE != undefined) {
        mapE.removeLayer(theMarkerE);
    };
    theMarkerE = L.marker([e.latlng.lat, e.latlng.lng],{
        draggable: true
      }).addTo(mapE);
}

map.on('click', onMapClick);
mapE.on('click', onMapClickE);

var dasarStyle = {
    "color": "blue",
    "fillColor": "white",
    "weight": 2,
    "opacity": 1,
    "fillOpacity": 0.3,
    "dashArray": '1, 5',
    "dashOffset": '0'
};

var kphStyle = {
    "color": "black",
    "fillColor": "green",
    "weight": 1,
    "opacity": 1,
    "fillOpacity": 0.3,
    "dashArray": '0',
    "dashOffset": '0'
};

function getPolyDasar(url,styleData,mapIn) {
    $.ajax({
        'type': "GET",
        'url': url,
        'data': null,
        'success': function (data) {
            //console.log(data);
            loadGeojson(data,styleData,mapIn);
            getPoly($('body').attr('data-asset-path') + 'map_poly',kphStyle,mapIn);
        }
    });
}

function getPoly(url,styleData,mapIn) {
    $.ajax({
        'type': "GET",
        'url': url,
        'data': null,
        'success': function (data) {
            //console.log(data);
            loadGeojson(data,styleData,mapIn);
        }
    });
}

function loadGeojson(str,styleData,mapIn) {
    geojson = new L.geoJson(str, {
        style: styleData,
        onEachFeature: onEachFeature
    }).addTo(mapIn);
    //map.fitBounds(geojson.getBounds());
    props = null;
    //map.setView([-2.7235830833483856, 112.50000000000001], 5.0);
    $(".info").text('Informasi');
}


function onEachFeature(feature, layer) {
    layer.on('click', function (e) {
        //infoKonten.update('<img src="http://localhost:8080/img/loading.gif">');
        if (feature.geometry['type'] != 'Point') {
            //map.fitBounds(layer.getBounds());
            if (feature.properties.Provinsi != null) {
                //getKonten();
            } else {
                //infoKonten.update('');
            }
        } else {
            //getKontenP();
        }
    });
}

function style(feature) {
    return {
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.6,
        fillColor: feature.properties.fillcolor
    };
}

getPolyDasar($('body').attr('data-asset-path') + 'js/leaflet/diy.json',dasarStyle,map);
getPolyDasar($('body').attr('data-asset-path') + 'js/leaflet/diy.json',dasarStyle,mapE);

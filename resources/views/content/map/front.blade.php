@php
$configData = Helper::applClasses();
@endphp
@extends('layouts/fullLayoutMaster')

@section('title', 'Coming Soon')

@section('page-style')
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-misc.css')) }}">
<link rel="stylesheet" href="{{asset('js/leaflet/leaflet.css')}}">
<link rel="stylesheet" href="{{asset('js/leaflet/css/MarkerCluster.Default.css')}}">
<link rel="stylesheet" href="{{asset('js/leaflet/css/MarkerCluster.css')}}">
<style>
    #sidebar {
        z-index: 9999;
        overflow-y: scroll;
        opacity: 0.9;
    }

    @media screen and (min-width: 30em) {
        #sidebar {
            max-height: calc(100vh - 5rem);
            height: auto;
            width: 460px;
        }
    }

    #gallery {
        position: relative;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        border: 1px solid rgb(200, 200, 200);
        font-size: 0.8rem;
        text-align: left;
    }

    td,
    th {
        border: 1px solid rgb(65, 50, 50);
        padding: 10px 20px;
    }

    th {
        background-color: rgb(255, 255, 255);
    }

    td {
        text-align: left;
    }

    .cari {
        padding: 6px 8px;
        font: 12px Arial, Helvetica, sans-serif;
        background: white;
        background: rgba(255, 255, 255, 0.8);
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
        border-radius: 5px;
    }

    .cari h4 {
        font-size: 14px;
        margin: 0 0 5px;
        color: #777;
    }

    .headerInfo {
        padding: 6px 8px;
        width: 300px;
        font: 12px Arial, Helvetica, sans-serif;
        background: white;
        background: rgba(255, 255, 255, 0.8);
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
        border-radius: 5px;
    }

    .headerInfo h4 {
        font-size: 16px;
        margin: 10px 5px 5px 50px;
        color: #000;
    }

    .headerInfo img {
        margin: 5px 10px 5px 5px;
    }

    .infoKonten {
        padding: 6px 8px;
        font: 12px Arial, Helvetica, sans-serif;
        background: white;
        background: rgba(255, 255, 255, 0.8);
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
        border-radius: 5px;
    }

    .infoKonten h4 {
        font-size: 14px;
        margin: 0 0 5px;
        color: #777;
    }

    .info {
        padding: 6px 8px;
        font: 14px/16px Arial, Helvetica, sans-serif;
        background: white;
        background: rgba(255, 255, 255, 0.8);
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
        border-radius: 5px;
    }

    .info h4 {
        font-size: 14px;
        margin: 0 0 5px;
        color: #777;
    }

    .legend {
        text-align: left;
        line-height: 18px;
        color: #555;
    }

    .legend i {
        width: 18px;
        height: 18px;
        float: left;
        margin-right: 8px;
        opacity: 0.7;
    }

    .ppprop {
        font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
        font-size: 12px;
        letter-spacing: 0px;
        word-spacing: 0px;
        color: #777;
        font-weight: normal;
        text-decoration: none;
        font-style: normal;
        font-variant: normal;
        text-transform: none;
    }

    .basemap {
        min-height: 100%;
        position: relative;
        background: white;
        background: rgba(255, 255, 255, 0.8);
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
        border-radius: 5px;
        padding: 6px 8px;
        bottom: 0;
        /* Firefox */
        min-height: -moz-calc(100% - 30px);
        /* WebKit */
        min-height: -webkit-calc(100% - 30px);
        /* Opera */
        min-height: -o-calc(100% - 30px);
        /* Standard */
        min-height: calc(100% - 30px);
    }

    body {
        margin: 0;
        padding: 0;
    }

    @keyframes fade {
        from {
            opacity: 0.5;
        }
    }

    .blinking {
        animation: fade 1s infinite alternate;
    }

    .select2-results__options {
        font-size: 12px !important;
    }
</style>
@endsection

@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="">
    <div class="container-fluid">
        <a class="navbar-brand" href="#"><!--<img class="logo" style="float: left;" src="{{ url('/uploads/') }}/diy.png" width="25px">--><h4>DATA DAN INFORMASI KPH PROVINSI DIY</h4></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        Dropdown
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div id="map" style="border:0px solid #0b0;height:100vh;width:100%;z-index: 1;"></div>
@endsection

@section('page-script')
{{-- Page js files --}}
<script src="{{ asset('js/leaflet/leaflet.js') }}"></script>
<script src="{{ asset('js/leaflet/leaflet.markercluster.js') }}"></script>
<script src="{{ asset('js/map/front.js') }}"></script>
@endsection
<div id="sidebar">
  <article id="placeInfo" class="" style="opacity: 1;">
    <h3>Detail Informasi</h3>
    <div id="description">
      <table>
        <tbody>
        <tr>
            <th scope="row">uID</th>
            <td>{{ $data->pohon_id }}</td>
          </tr>
          <tr>
            <th scope="row">Nama Pohon</th>
            <td>{{ $data->pohon_nama }}</td>
          </tr>
          <tr>
            <th scope="row">Jenis</th>
            <td>{{ $data->pohon_jenis }}</td>
          </tr>
          <tr>
            <th scope="row">Diameter</th>
            <td>{{ $data->pohon_diameter }} cm</td>
          </tr>
          <tr>
            <th scope="row">Tinggi</th>
            <td>{{ $data->pohon_tinggi }} cm</td>
          </tr>
          <tr>
            <th scope="row">Status</th>
            <td>{{ $data->pohon_status }}</td>
          </tr>
          <tr>
            <th scope="row">Tahun Tanam</th>
            <td>{{ $data->pohon_tahun_tanam }}</td>
          </tr>
          <tr>
            <th scope="row">Logitude</th>
            <td>{{ $data->pohon_lat }}</td>
          </tr>
          <tr>
            <th scope="row">Latitude</th>
            <td>{{ $data->pohon_long }}</td>
          </tr>
          <tr>
            <th scope="row">QrCode</th>
            <td>{!! $data->qr !!}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <h3>Foto Pohon</h3>
    <div id="gallery">
        <img src="{{ url('/uploads/pohon') }}/{!! $data->pohon_img !!}" width="100%">
      </a>
    </div>
  </article>
  <footer class="black-50 f6 pv3"> Referensi: DLHK Daerah Istimewa Yogyakarta </footer>
</div>
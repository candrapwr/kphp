<div id="sidebar">
  <article id="placeInfo" class="" style="opacity: 1;">
    <h3>Informasi KPH</h3>
    <div id="description">
    <table>
        <tbody>
          <tr>
            <th scope="row">Provinsi</th>
            <td>Daerah Istimewa Yogyakarta</td>
          </tr>
          <tr>
            <th scope="row">Kesatuan Pengelolaan Hutan (KPH) </th>
            <td>{{ $data->kph_name }}</td>
          </tr>
          <tr>
            <th scope="row">Bagian Daerah Hutan (BDH)</th>
            <td>{{ $data->bdh_name }} </td>
          </tr>
          <tr>
            <th scope="row">Resort Pengelolaan Hutan (RPH)</th>
            <td>{{ $data->rph_name }}</td>
          </tr>
          <tr>
            <th scope="row">Luas RPH</th>
            <td>{{ $data->rph_size }} Ha</td>
          </tr>
          <tr>
            <th scope="row">SK MDL</th>
            <td>-</td>
          </tr>
          <tr>
            <th scope="row">Tanggal SK MDL</th>
            <td>-</td>
          </tr>
          <tr>
            <th scope="row">Jumlah Pohon</th>
            <td>{{ $data->total_pohon }} Pohon</td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
  </article>
  <footer class="black-50 f6 pv3"> Referensi: DLHK Daerah Istimewa Yogyakarta </footer>
</div>